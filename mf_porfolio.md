## Assignment 1-

Design a system for a bank to lend money to borrowers and receive payment for the loans.

The system should have the following functions:

LEND: The bank can give loans to customers. There is no restriction on the loan amount and number of loans given to a customer. The function should take customer_id, loan_amount(P), loan_period(N) and rate of interest(I) as input. It should return the Total amount(A) to be paid and the monthly EMI to be paid.

PAYMENT: Customers can pay back loans either in the form of EMI or through LUMP SUM amounts. In the case of Lump sum payment, the lump sum amount will get deducted from the total amount. This can reduce the number of EMIs.

LEDGER: Customers can check all the transactions for a loan id. Along with all the transactions, It should also return the balance amount, monthly EMI and number of EMI left.

ACCOUNT OVERVIEW: This should list all the loans customers have taken. For each loan, it should tell the loan amount(P), Total amount(A), EMI amount, Total Interest(I), the amount paid till date, number of EMI left.

Calculations:

I(Interest) = P (Principal) * N (No of Years) * R (Rate of interest)

A(Total Amount) = P + I

Assumption:

Feel free to take any assumption

Note:
* Use restful APIs to expose the functions.
* You can use any programming language and framework of your choice.
* To persist data use any database / file based system.
* Keep things simple. The system should not be at the production level. But you should be able to explain your design decision.

The code will be judged based on the quality, implementation and originality of the solution.


## Assignment 2-

Create a frontend Web application to create a mutual fund portfolio. Users should be able to 

1. Search mutual funds.
2. When the user selects the mutual fund the mutual fund should appear in the portfolio list with default 1 unit issued.
3. Users can increase or decrease the number of units of a mutual fund.
4. When a user selects a mutual fund in the portfolio list the details of the mutual fund should appear in a pop-up.
5. The pop up should show the following scheme details-
    * scheme name 
    * fund house
    * Scheme category 
    * Scheme type
    * Latest nav
    * Latest nav date
6. For simplicity, selling and buying will happen in the number of units and not in money.
2. The portfolio page should list all mutual funds and total units currently available in the portfolio.

Color Scheme(Feel free to use any additional colors you need):

	primary color- #346ccb

	secondary color- #fbfcfd

Use below APIs to
* List [https://api.mfapi.in/mf](https://api.mfapi.in/mf) 
* Search [https://api.mfapi.in/mf/search?q=sbi](https://api.mfapi.in/mf/search?q=sbi)
* Fund details [https://api.mfapi.in/mf/](https://api.mfapi.in/mf/)&lt;scheme_code>

You can refer to the wireframes below for UI. Feel free to apply any modifications if you want.

We prefer you to use any modern web framework like react js, Vue js, Angular etc. But you can use plain Javascript also if you prefer.

Wireframes

![alt_text](https://i.imgur.com/bUAOwjA.png "image_tooltip")


![alt_text](https://i.imgur.com/a4Bo14i.png "image_tooltip")

