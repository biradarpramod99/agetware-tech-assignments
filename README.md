# AgetWare Web Developer Internship Assignment

This repository contains different problems with statements.

You can choose to take up fullstack problems as a whole or separately into frontend and backend. Extra weightage will be given if a person solves atleast one frontend and one backend problem. Person will also be assessed on how many problems they solve - more problems solved will be given more preference.

Code will be assessed based on the following factors:
- Structure
- Readability
- Optimisation
- Approach to solve the problem
- Working condition with the correct output
